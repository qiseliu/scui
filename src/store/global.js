import config from "@/config";
import { defineStore } from "pinia";
import storage from "@/utils/tool";

//转换
function getInitialCount() {
	if (storage.data.get("LAYOUT_TAGS") == null || storage.data.get("LAYOUT_TAGS") == 'null') {
		return config.LAYOUT_TAGS;
	} else {
		return storage.data.get("LAYOUT_TAGS");
	}
}
export const useGlobalStore = defineStore('globalStore', {
	state: () => ({
		//移动端布局
		ismobile: false,
		//布局
		layout: storage.data.get("LAYOUT") || config.LAYOUT,
		//菜单是否折叠 toggle
		menuIsCollapse: config.MENU_IS_COLLAPSE,
		//多标签栏
		layoutTags: getInitialCount(),
		//主题
		theme: config.THEME,
	}),
	getters: {

	},
	actions: {
		setAppLayout(layout) {
			this.layout = layout
			storage.data.set("LAYOUT", this.layout);
		},
		setLayoutSaveTags(layoutSaveTags) {
			this.layoutTags = layoutSaveTags;
			storage.data.set("LAYOUT_TAGS", layoutSaveTags);
		},
		setIsMobile(key){
			this.ismobile = key
		},
		toggleMenuIsCollapse() {
			this.menuIsCollapse = !this.menuIsCollapse
		},
	},
})
