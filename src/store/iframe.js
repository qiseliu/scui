import { defineStore } from "pinia";

export const useIframeStore = defineStore('iframeStore', {
	state: () => ({
		iframeList: []
	}),
	getters: {

	},
	actions: {
		setIframeList(route){
			this.iframeList = []
			this.iframeList.push(route)
		},
		pushIframeList(route){
			let target = this.iframeList.find((item) => item.path === route.path)
			if(!target){
				this.iframeList.push(route)
			}
		},
		clearIframeList(){
			this.iframeList = []
		},
		refreshIframe(route){
			this.iframeList.forEach((item) => {
				if (item.path == route.path){
					var url = route.meta.url;
					item.meta.url = '';
					setTimeout(function() {
						item.meta.url = url
					}, 200);
				}
			})
		},
		removeIframeList(route){
			this.iframeList.forEach((item, index) => {
				if (item.path === route.path){
					this.iframeList.splice(index, 1)
				}
			})
		},
	},
})