import { createApp } from 'vue'
import { createPinia } from 'pinia'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import scui from './scui'
import i18n from './locales'
import router from './router'
// import store from './store'
import App from './App.vue'
import 'uno.css'

const pinia = createPinia();
const app = createApp(App);

app.use(pinia)
// app.use(store)
app.use(router);
app.use(ElementPlus);
app.use(i18n);
app.use(scui);

//挂载app
app.mount('#app');
