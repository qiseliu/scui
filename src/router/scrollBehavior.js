import { nextTick } from 'vue'
import { useViewTagsStore } from '@/store/viewTags'

export function beforeEach(to, from) {
	const viewTags = useViewTagsStore();
	var adminMain = document.querySelector('#adminui-main')
	if (!adminMain) { return false }
	viewTags.updateViewTags(from.fullPath)
}

export function afterEach(to) {
	const viewTags = useViewTagsStore();
	var adminMain = document.querySelector('#adminui-main')
	if (!adminMain) { return false }
	nextTick(() => {
		var beforeRoute = viewTags.viewTags.filter(v => v.fullPath == to.fullPath)[0]
		if (beforeRoute) {
			adminMain.scrollTop = beforeRoute.scrollTop || 0
		}
	})
}