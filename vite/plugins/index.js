import vue from '@vitejs/plugin-vue'
import createCompression from './compression'
// 引入unocss引擎
import Unocss from 'unocss/vite'
import createAutoImport from "./auto-import"
import createComponents from "./components"
import createSetupExtend from "./setup-extend"

export default function createVitePlugins(viteEnv, isBuild = false) {
    const vitePlugins = [vue()]
    vitePlugins.push(createAutoImport())
    vitePlugins.push(createSetupExtend())
    vitePlugins.push(createComponents(viteEnv))
    vitePlugins.push(Unocss())
	isBuild && vitePlugins.push(...createCompression(viteEnv))
    return vitePlugins
}
